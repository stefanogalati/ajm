jira.inup_win
=============

This is the role for installation and upgrade procedures in Windows hosts.

Requirements
------------

Any pre-requisite is demanded for this role.

Role Variables
--------------

All role variables are setted into `/defaults/main.yml` file

```yaml
---
jira_service_name: 'Atlassian JIRA'
jira_home_win: 'C:\Program Files\Atlassian\Application Data\JIRA'
jira_install_win: 'C:\Program Files\Atlassian\JIRA_8.6.0'
jira_install_abs_win: 'C:\Program Files\Atlassian'
jira_bkp_win: 'C:\JIRA_BACKUP'
jira_temp_win: 'C:\JIRA_TEMP'
ajm_java_folder: 'C:\Program Files\JAVA'

# variables used to chose the type and version of the Jira application
jira_exec_win: atlassian-jira-{{ jira_type }}-{{ jira_version }}.zip
jira_download_win: https://www.atlassian.com/software/jira/downloads/binary/{{ jira_exec_win }}

install_jdk: True
```
